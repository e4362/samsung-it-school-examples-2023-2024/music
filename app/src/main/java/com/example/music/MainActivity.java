package com.example.music;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.list_view);

        String[] names = {"Numb", "Hero", "We're Not Gonna Take It",
        "Дай мне посмотреть", "Кукушка"};
        String[] albums = {"Meteora", "Awake", "Stay Hungry",
        "Сингл", "Черный альбом"};
        String[] authors = {"Linkin Park", "Skillet", "Twisted Sisters",
        "Heronwater", "КИНО"};
        int[] lengths = {187, 186, 219, 125, 268};
        String[] explited = {"", "", "", "E", ""};
        String[] URLs = {"https://music.yandex.ru/album/21940/track/178532",
                "https://music.yandex.ru/album/2259369/track/418017",
                "https://music.yandex.ru/album/1893146/track/150854",
                "https://music.yandex.ru/album/26221804/track/114664869",
                "https://music.yandex.ru/album/10373/track/38633759"};
        int[] photoIds = {R.drawable.p1, R.drawable.p2, R.drawable.p3,
        R.drawable.p4, R.drawable.p5};

        ArrayList<HashMap<String, Object>> data = new ArrayList<>();

        for (int i = 0; i < names.length; i++){
            HashMap<String, Object> map = new HashMap<>();
            map.put("name", names[i]);
            map.put("album", albums[i]);
            map.put("authors", authors[i]);
            map.put("length", lengths[i] / 60 + ":"  + lengths[i] % 60);
            map.put("explited", explited[i]);
            map.put("photo", photoIds[i]);
            data.add(map);
        }
        String[] from = {"name", "album", "authors", "length", "explited", "photo"};
        int[] to = {R.id.textView, R.id.textView2, R.id.textView3, R.id.textView4,
                R.id.textView5, R.id.imageView};

        SimpleAdapter simpleAdapter = new SimpleAdapter(this,
                data, R.layout.list_item, from, to);
        listView.setAdapter(simpleAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = URLs[position];
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);

            }
        });
    }
}